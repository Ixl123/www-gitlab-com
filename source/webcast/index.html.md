---
layout: markdown_page
title: "Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts  

### GitLab for Enterprises
This is a reoccuring demo webcast that is held every two weeks on Wednesday morning. You'll get a first-hand look at how GitLab integrates version control, project management, code review, testing, and deployment to help enterprise teams move faster from planning to monitoring.   

We are currently changing the layout of this webcast series. There are no future dates scheduled at this time. Please check back in the near future. 
   
   

### Overcoming Barriers to DevOps Automation - a 4-part series
**April 25th**: Let's Talk DevOps and Drawbacks   
**May 9th**: Solving the Collaboration Conundrum: How to Make the DevOps Dream a Reality    
**May 23rd**: Removing Barriers Between Dev and Ops: Setting Up a CI/CD Pipeline   
**June 6th**: Getting Started with CI/CD on GitLab   

Each webcast starts at 9am PDT / 4pm UTC, you can [register](/webcast/devops-automation/) for one or the entire series.

   