---
layout: markdown_page
title: "Technical Interviews"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Technical interview<a name="technical-interview"></a>

For some positions, the hiring process includes a technical interview.

This technical interview revolves around a feature request from the
[GitLab Community Edition issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)
that you will be asked to implement, with guidance from the interviewer(s).

(Don't feel like reading all of the below, or just looking for a refresher?
We've got you covered; there's a [TL;DR](#tldr) at the bottom!)

This technical interview starts with a 1-hour video call and screen sharing
session with the interviewer(s), where they will walk you through an issue they
selected, and where you will make a start on the feature with their guidance.

To make the best use of our limited time available together, you are expected to
have set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
beforehand. Once it is set up, you can make sure it's good to go by logging in
as the root user (using the password in the README) and checking that you see
projects and issues. Don't underestimate the time it can take to get the GDK up
and running with all of the seed data; it is strongly recommended that you start
setting up the GDK a few hours before the interview, or even the day before.

The goal of this hour is to get you to a place where you feel comfortable enough
with the requirements of the feature, its context inside GitLab, as well as the
relevant parts of the code base, to finish implementing the feature by yourself,
asynchronously. You lead the hour, and the interviewers are there to answer any
questions you might have, or to nudge you in the right direction if they see
that you're stuck or getting off track. We realize that GitLab is a complex beast,
and we're here to help you tame it.

If you get the hang of the feature and the code base quickly, you can spend the
remainder of the hour working on the actual implementation with guidance
from the interviewers (but not pair programming-style help), but this is not
required or expected.

After this synchronous component of the interview, you will be asked to spend 4
hours of your own time on implementing the feature, and to submit a merge
request with the changes to the GitLab CE project. Because of the limited time
we want you to spend on this first iteration, we expect the MR to be submitted by
the start of the next work week, at the latest. Let us know if this is not workable for you.

If there are certain things missing from the first iteration of the MR that you
would have added if you had had more time, please list these things explicitly
when you submit it. Think of things like edge cases that are not covered, code
that could be cleaned up or refactored, specs that have yet to be written, or
any part of the scope of the feature as it was discussed during the 1-hour call
that you simply didn't get to yet. Be sure to check out the task list in the MR
description template, as well as our [definition of done](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#definition-of-done).

When you submit your MR, the interviewer(s) will review it, and leave any feedback
they may have. They may have questions or suggestions regarding the code you have
written, or they may ask you to spend some more time to implement one of the items
you had identified as missing. They may also ask you about something that was not
part of the original scope of the feature, to get you to think about a potential
future iteration of the feature, how it could be implemented, and what that may
mean for the implementation of the feature you are working on right now.

After this first round of review, you will be expected to spend up to 2
hours to respond to any questions, address any concerns, and implement anything
that was missing from the first iteration. You are expected to prioritize the
specific things reviewer(s) asked for, but you can spend this time to improve
upon the first iteration in any way you see fit.

After this second iteration, the reviewer(s) will do a second round of review,
leaving more questions and feedback. Again, you are expected to spend up to 2
hours to respond to and/or address the latest feedback in a third iteration.
If you reach the time limit before addressing all outstanding discussions,
we would appreciate it if you spent a few moments to respond to each of them with
some notes on how you would resolve it, if you had the time, so that we have a
complete picture of what you think of the feature, the MR, and where it could go
from here. Then, please let us know that you've reached the time limit, in the MR
or via email.

At this point, the interviewer(s) will evaluate your candidacy based on the
final state of the merge request, the state after each iteration, and your
communication and collaboration throughout the process.

Our technical interview looks like this because we believe that it is the best
way for you to see what the work is really like, and for us to see how you [think, code, and collaborate](http://zachholman.com/posts/startup-interviewing-is-fucked/#collaborate).

If you would prefer not to follow the above process, please let us know and
we'll find an alternative task. If you are concerned with the public nature of it,
a potential alternative would be to work on a GitLab CE issue on a private fork,
in a private merge request.

After all of this, and whether or not you end up getting hired, we would be more
than happy to continue to work with you to get the MR ready to be merged into GitLab CE.
If you don't have the time or interest to do so, we can also take over the MR and finish it
in your stead. You would of course get credit for your contribution in the MR,
the changelog, and (depending on the feature) the eventual blog post for the
release this feature will go into.

Once the merge request is finished, by you or by us, the code you have written will
go into the
[GitLab Community Edition](/features/#community) to the
benefit of the millions of developers using this free, open-source product,
but will also go into the proprietary
[GitLab Enterprise Edition](/features/#enterprise),
which is a fork thereof.

If you would prefer that the code that you have written for the technical interview
does not end up in GitLab CE or GitLab EE at all, you are free to close the merge
request (or delete the source branch or your fork) at this stage.

When contributing code, you should follow the [Contribution guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md),
and you agree to the [individual contributor license agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/individual_contributor_license_agreement.md).

## TL;DR

The technical interview in a nutshell:

1. At least a day before the interview, set up the [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit).
1. A one-hour call where we will share a [GitLab CE issue](https://gitlab.com/gitlab-org/gitlab-ce/issues) with you, and help you get started on the implementation.
1. Spend up to four hours to submit the initial MR by the start of the following work week.
1. We will review the MR.
1. Spend up to two hours addressing and responding to those review comments.
1. We will review the MR for a second time.
1. Spend up to two hours addressing and responding to those review comments.
1. After the third round of review, we'll make our evaluation and get back to you with the next steps.
