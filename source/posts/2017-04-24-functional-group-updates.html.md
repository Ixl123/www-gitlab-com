---
title: "GitLab's Functional Group Updates - April 10th - 20th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Kirsten Abma
author_gitlab: kirstenabma
categories: GitLab news
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://smcgivern.gitlab.io/discussion-updates/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nSWLHIbwioc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://www.slideshare.net/JobvanderVoort/gitlab-product-update-april-11-2017)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qGguRHwTTMQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### PeopleOps Team

[Presentation slides](https://docs.google.com/presentation/d/1WLV7NCLbQTMGHyNh_b0AzeHM2VYNZMfb7EWG-sWKVdE/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/t90wqyDAPQk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----


Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
