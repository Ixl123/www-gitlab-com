---
title: "GitLab's Functional Group Updates: July 3rd - July 26th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: GitLab news
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1L-cljaf8ZBh4Zd-09QvOwwFewP-KljglPRxATsBJJy8/edit#slide=id.p)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/vkgdc8MvcCc" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI Team

[Presentation slides](https://docs.google.com/presentation/d/16gID7ujFjttCnm2lKc5TYTCiKHDan88QXYic6IQPepY)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KqbH2yrBtE4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Build Team

[Presentation slides](https://docs.google.com/presentation/d/1wMB3H3L722aGwOm6tm3R_yzSMACzs_5UIWHS8EBxBg4/edit#slide=id.g153a2ed090_0_63)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gN1_Zr5a45Q" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/presentation/d/17zIU8-VriL_SDGyWXqoKyVRbe_WIwi2ag92IcR23-Wo/edit#slide=id.g23210ddca3_0_9)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/h-4w23182pk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1mheeA7DWE4yMOMvqFTCplMlf2MwY4YyfY7QqGb2QywA/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cULFKBf9Sfo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Engineering Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1bRFNRZ1q6bWSKDtYEWva6ke5YsZQXNtIVvLrswA7GsA/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/9RuK4eVJpU8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](http://gitlab-org.gitlab.io/functional-group-updates/edge/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cQePZZHHGwY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1j43_9ElQjON8I5zM8xd9WiZQBkz7hqiYOqU1M8TGx4w/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/FNG9tJZ3yOQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### People Ops Team

[Presentation slides](https://docs.google.com/presentation/d/10mS2AvTt1rU3A1e_llbNl2pVLxzAdbIX0ovOQ_8QyPE/edit#slide=id.g24ad25762a_0_5)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/df2rXVYdEbY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----


Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
