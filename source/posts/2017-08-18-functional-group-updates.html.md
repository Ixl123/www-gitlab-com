---
title: "GitLab's Functional Group Updates: August 14th - August 18th" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: GitLab news
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI/CD Team

[Presentation slides](https://docs.google.com/presentation/d/110g7Fnd3jyfHGqVVQXi0C_fjjylxvF6EAl9EalJZ78A/edit#slide=id.g1d369b56b4_4_12)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/72vmpZo74Jc" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1qjvnfrwueG9kxsIPbGdxVmcfX1pwX2XAxGCtyHPiRXs/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tNtuWrcCQvI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/12BYNEyT30i6m1nVytiPH5uK3uY8PBt24R-auGWEqBV0/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TgPepw7GoQ0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
