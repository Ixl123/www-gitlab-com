---
title: "GitLab's Functional Group Updates: January 3rd - January 31st" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Chloe Whitestone
author_gitlab: chloemw
author_twitter: drachanya
categories: GitLab news
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/functional-group-updates/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1VGIVnoBvEUPMyRYtQ21WrKL79PTC7fHhprkgFsMwGl4/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ZVPIEQXl2CU" frameborder="0" allowfullscreen="true"></iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/16aPs_60brJFKYyJZJNMweQDOc2NsnfeWer9hPwBPJzo/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/rP69Qa4LGCg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Edge Team

[Presentation slides](http://gitlab-org.gitlab.io/functional-group-updates/edge/2018-01-09/#1)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nUJu2dL1JHo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Research Team

[Presentation slides](https://docs.google.com/presentation/d/1-E-Y-6-_C0sEUInf-bbek2DOwQM802aM-ZP1crMxkiU/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/_TAOmK-ypvA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Discussion Team

[Presentation slides](http://gitlab-org.gitlab.io/functional-group-updates/backend-discussion/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nT7fX4O6Urs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Content Marketing Team

[Presentation slides](https://docs.google.com/presentation/d/1qGK8oc9NcJ92fEVuvMrGOPNgjvCZ_UxzKTQZ61n_UtQ/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/pOplmf3WnvQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Platform Backend Team

[Presentation slides](https://docs.google.com/presentation/d/1orvmzwWecUWxRmL0DTJzW4C24aKJCsjnL-PQtxIsUAA/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/20V0laoBE9c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### UX Team

[Presentation slides](https://docs.google.com/a/gitlab.com/presentation/d/1cT6lPbba81ckN04JW55gVU_TkuxWOlvH9-9-KPGSZPA/edit?usp=drive_web)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NOsMqyLVBww" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Build Team

[Presentation slides](https://docs.google.com/presentation/d/1YLxJS4iic9nTGxWbaz8IMVkoWQfy2RVJ95fR2Jcze5c/edit#slide=id.g2823c3f9ca_0_0)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/iWTIotCxTa4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
