---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-05-05.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 262   | 100%        |
| Based in the US                           | 146  |  55.73%      |
| Based in the UK                           | 18    | 6.87%      |
| Based in the Netherlands                  | 11    | 4.20%       |
| Based in Other Countries                  | 87    | 33.21%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 262   | 100%        |
| Men                                       | 208   | 79.39%      |
| Women                                     | 54    | 20.61%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Men in Leadership                         | 20    | 80.00%      |
| Women in Leadership                       | 5     | 20.00%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 114   | 100%        |
| Men in Development                        | 101   | 88.60%      |
| Women in Development                      | 13    | 11.40%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 146   | 100%        |
| Asian                                     | 11    | 7.25%       |
| Black or African American                 | 3     | 2.05%       |
| Hispanic or Latino                        | 10    | 6.85%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.68%       |
| Two or More Races                         | 4     | 2.74%       |
| White                                     | 79    | 54.11%      |
| Unreported                                | 38    | 26.03%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 34    | 100%        |
| Asian                                     | 4     | 11.76%      |
| Black or African American                 | 1     | 2.94%       |
| Two or More Races                         | 1     | 2.94%       |
| White                                     | 16    | 47.04%      |
| Unreported                                | 9     | 26.47%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 21    | 100%        |
| Asian                                     | 2     | 9.52%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.76%       |
| White                                     | 11    | 52.38%      |
| Unreported                                | 7     | 33.33%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 262   | 100%        |
| Asian                                     | 17    | 6.49%       |
| Black or African American                 | 6     | 2.29%       |
| Hispanic or Latino                        | 16    | 6.11%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.38%       |
| Two or More Races                         | 5     | 1.91%       |
| White                                     | 140   | 53.44%      |
| Unreported                                | 77    | 29.39%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 114   | 100%        |
| Asian                                     | 8     | 7.02%       |
| Black or African American                 | 3     | 2.63%       |
| Hispanic or Latino                        | 7     | 6.14%       |
| Two or More Races                         | 2     | 1.75%       |
| White                                     | 56    | 49.12%      |
| Unreported                                | 37    | 32.46%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 25    | 100%        |
| Asian                                     | 2     | 8.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.00%       |
| White                                     | 11    | 44.00%      |
| Unreported                                | 10    | 40.00%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 262   | 100%        |
| 18-24                                     | 15    | 5.73%       |
| 25-29                                     | 66    | 25.19%      |
| 30-34                                     | 72    | 27.48%      |
| 35-39                                     | 41    | 15.65%      |
| 40-49                                     | 44    | 16.79%      |
| 50-59                                     | 22    | 8.40%       |
| 60+                                       | 2     | 0.76%       |
| Unreported                                | 0     | 0.00%       |
