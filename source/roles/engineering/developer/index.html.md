---
layout: job_page
title: "Developer"
---

# Developers Roles at GitLab

Developers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and backend or frontend developers to solve common goals.

Unless otherwise specified, all Developer roles at GitLab share the following
requirements and responsibilities:

#### Requirements
* Significant experience with Ruby and Rails\*
* Positive and solution-oriented mindset
* English written and verbal communication skills
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization
* Self-motivated and self-managing, with strong organizational skills.

#### Responsibilities
* Constantly improve product quality, security, and performance
* Write good code
* Catch bugs and style issues in code reviews
* Ship small features independently

***

\* - unless otherwise specified below, please note that a significant amount of experience with Ruby is a **strict**
requirement.

We would love to hire all great backend developers, regardless of the language
they have most experience with, but at this point we are looking for developers
who can get up and running within the GitLab code base very quickly and without
requiring much training, which limits us to developers with a large amount of
existing experience with Ruby, and preferably Rails too.

For a time, we also considered candidates with little or no Ruby and Rails
experience for this position, because we realize that programming skills are to
a large extent transferable between programming languages, but we are not
currently doing that anymore for the reasons described in the [merge
request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695) that
removed the section from this listing that described that policy.

***

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

### Junior Developer

Junior Developers share the same requirements and responsibilities outlined above, but typically join with less or alternate experience than a typical Developer.

### Senior Developer

* Write _great_ code
* Teach and enforce architectural patterns in code reviews
* Ship _medium_ features independently
* Generate architecture recommendations
* _Great_ communication: Regularly achieve consensus amongst _teams_
* Perform technical interviews

***

A Senior Developer may want to pursue the [engineering management track](/roles/engineering/engineering-management) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

**Note:** Staff and above positions at GitLab are more of a role than just a "level". We prefer to bring people in as Senior and let the team elevate them to staff due to an outstanding work history within GitLab.

***

### Staff Developer

The Staff Developer role extends the [Senior Developer](#senior-developer) role.

* Write _exquisite_ code
* Ship _large_ features independently
* Make architecture decisions
* Implement technical and process improvements
* _Exquisite_ communication: Regularly achieve consensus amongst _departments_
* Author technical architecture documents for epics
* Train others to perform technical interviews and author code tests (where applicable)
* Write public blog posts

### Distinguished Developer

The Distinguished Developer role extends the [Staff Developer](#staff-developer) role.

* Ship _large_ feature sets with team
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for developers
* Author architecture documents for epics
* Hold team members accountable within their roles

### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Developer](#distinguished-developer) role.

* Work on our hardest technical problems
* Ship _extra-large_ feature sets with team
* Help create the sense of psychological safety in the department
* Represent the company publicly at conferences
* Work directly with customers

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Platform / Discussion / Monitoring

The [Platform](/handbook/backend/#platform),
[Discussion](/handbook/backend/#discussion), and
[Monitoring](/handbook/backend/#monitoring) teams are
responsible for working on the main Rails application in GitLab, with different
focuses. These teams do very similar work, and share a hiring pool.

### Distribution

The Distribution team closely partners with the rest of the engineering organization to build, configure, and automate GitLab installation.
GitLab's distribution team is tasked with creating a seamless installation experience for customers and community users across multitude of platforms.

Distribution engineering is interlaced with the broader development team in supporting newly created features.
Notably, the infrastructure team is the distribution team's biggest internal customer, so there is significant team interdependency.
The Distribution team also provides significant variety in tasks and access to a diversity of projects, including helping out on various community packaging projects.
This is reflected in the job requirements: we are tasked with creating and maintaining a Cloud Native GitLab deployment and upgrade methods, Omnibus GitLab package installation across multiple Linux based Operating Systems, and various Cloud providers deployment methods (such as AWS Cloudformation, GC Deployment Manager and so on).

#### Requirements

* Experience with Docker and Kubernetes in production use cases
* Chef experience (writing complex cookbooks from scratch, custom providers, custom resources, etc.)
* Extensive Linux experience, comfortable between Debian and RHEL based systems
* Basic knowledge of packaging archives such as .deb and .rpm package archives

#### Cloud Native

This specialty expands on Distribution, with a specific focus on a
cloud native deployment methods and development. Cloud Native engineer is tasked
with designing, building and configuring deployment methods of GitLab for
Kubernetes. This specialty requires experience with:

* Running applications at scale in Kubernetes
* Transforming existing applications to cloud native oriented applications
* Designing, building and packaging cloud native applications

### Packaging

Packaging developers are focused on creating the binary repository management
system that will extend our Continuous Integration (CI) functionality to allow access
and management of artifacts manipulated by projects.

By extending the current CI artifacts system, the Packaging team will expose GitLab as
a package repository allowing access to the most common package managers, e.g.
Maven and APT and similar. Additionally, the Packaging team is improving
the Container Registry and is responsible for items listed under [Packaging product category](/handbook/product/categories/).

#### Requirements
* Experience with Go
* Experience with working and reasoning with applications running at scale
* Experience or strong interest in developing as a DevOps engineer
* Experience or strong interest in software packaging and package distribution systems

#### Responsibilities
* Develop the architecture by extending existing features
* Work with the Distribution team on replacing their current delivery system
* Create and maintain observability of the newly defined features
* Work with customers on defining their needs to replace existing package repository solutions

### Security Products

Focus on security features and security products for GitLab. This role will specifically focus on security; if you want to work with Ruby on Rails and not security, please apply to our Backend Developer role instead. This role will report to and collaborate directly with the Security Products Engineering Manager.

#### Requirements
* Strong Go and/or Ruby developer with security expertise or proven security interest.
* Passion and interest toward security (scanning, dependencies, etc.).
* Experience in using GitLab and GitLab CI.

#### Responsibilities
* Develop security products from proposal to polished end result.
* Integrating 3rd party security tools into GitLab.
* Complete our internal Advisories Database.
* Manage metadata related to dependencies.
* Key aspects of this role are focused on security products and features.
* The complexity of this role will increase over time.
* If you are willing to stick to working on these features for at least a year, then this role is for you.

### Configuration

  The configuration team works on GitLab's Application Control Panel, Infrastructure Configuration features, our ChatOps product, Feature flags, and our entire Auto DevOps feature set. It is part of our collection of Ops Backend teams.

#### Requirements
* Experienced engineer who is capable of leading and growing a team of senior engineers.
* Proficient in Golang.
* Experience with Docker, Kubernetes platform development.
* Experience or interest in functions-as-a-service.

#### Responsibilities
* Implement and improve upon our constellation of configuration feature set.
* Work with the PM team to execute on the roadmap.
* Ensure we deliver on our commitments to the market by communicating clearly with stackholders.
* Implement the appropriate monitoring and alerting on new and existing features owned by the team.
* Help others adopt and use the configuration features.

### CI/CD

CI/CD Backend Developers are primarily tasked with improving the Continuous Integration (CI)
and Continuous Deployment (CD) functionality in GitLab. Engineers should be willing to learn Kubernetes and Container Technology. CI/CD Engineers should always have three goals in mind:
1. Provide value to the user and communicate such with product managers,
2. Introduce features that work at scale and in untrusting environments,
3. Always focus on defining and shipping [the Minimal Viable Change](/handbook/product/#the-minimally-viable-change).

We, as a team, cover end-to-end integration of CI/CD in GitLab, with components being written in Rails and Go.
We work on a scale of processing a few million of CI/CD jobs on GitLab.com monthly.
CI/CD engineering is interlaced with a number of teams across GitLab.
We build new features by following our [direction](/direction/#ci--cd).
Currently, we focus on providing a deep integration of Kubernetes with GitLab:
1. by automating application testing and deployment through Auto DevOps,
1. by managing GitLab Runners on top of Kubernetes,
1. by working with other teams that provide facilities to monitor all running applications,
1. in the future implement A-B testing, feature flags, etc.

Additionally, we also focus on improving the efficiency, performance, and scalability of all aspects of CI/CD:
1. Improve performance of developer workflows, e.g. faster CI testing, by improving parallelization,
1. Improve performance of implementation, ex.:by allowing us to run 10-100x more in one year,
1. Identify and add features needed by us, ex.:to allow us to test more reliable and ship faster.

The CI/CD Engineering Manager also does weekly stand-up with a team and product managers to talk about plan for the work in the upcoming week and coordinates a deployment of CI/CD related services with infrastructure team.

#### Requirements
* Go developer with a lot of Kubernetes production experience is a plus

Candidates may be invited to schedule an interview with either an Engineer or Product Manager as part of the hiring process.

### Geo

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html)
is an enterprise product feature that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well.

#### Requirements
* Deep experience architecting and implementing fault-tolerant, distributed systems
* Experience building and scaling highly-available systems
* In-depth experience with Ruby on Rails, Go, and/or Git

#### Requirements (Staff Level)
* Architect Geo and Disaster Recovery products for GitLab
* Identify ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
* Instrument and monitor the health of distributed GitLab instances
* Educate all team members on best practices relating to high availability

### Quality

Quality Engineers are primarily tasked with improving the productivity of
the GitLab developers (from both GitLab Inc and the rest of the community), and
making the GitLab project maintainable in the long-term.

See the description of the [Quality team](/handbook/quality) for more details.
The position also involves working with the community as
[merge request coach](/roles/merge-request-coach), and working together with our
[issue triage specialists](/roles/specialist/issue-triage) to respond and
address issues from the community.

### Gitaly

Gitaly is a new service in our architecture that handles git and other filesystem operations for GitLab instances, and aims to improve reliability and performance while scaling to meet the needs of installations with thousands of concurrent users, including our site GitLab.com. This position reports to the Gitaly Lead.

#### Responsibilities
* Participate in architectural discussions and decisions surrounding Gitaly.
* Scope, estimate and describe tasks to reach the team’s goals.
* Collaborate on designing RPC interfaces for the Gitaly service
* Instrument, monitor and profile Gitaly in the production environment.
* Build dashboards and alerts to monitor the health of your services.
* Conduct acceptance testing of the features you’ve built.
* Educate all team members on best practices relating to high availability.

#### Requirements:
* Mandatory: production experience building, debugging, optimising software in large-scale, high-volume environments.
* Mandatory: Solid production Ruby experience.
* Highly desirable: Experience working with Go. It’s important that candidates must be willing to learn and work in both Go and Ruby.
* Highly desirable: experience with gRPC.
* Highly desirable: a good understanding of git’s internal data structures or experience running git servers. You can reason about software, algorithms, and performance from a high level.
* Understanding of how to build instrumented, observable software systems.
* Experience highly-available systems in production environments.

### BizOps

[BizOps](https://gitlab.com/gitlab-org/bizops) is an early stage project at GitLab focused on delivering an open source framework for analytics, business intelligence, and data science. It leverages version control, data science tools, CI, CD, Kubernetes, and review apps.

A BizOps developer will be tasked with executing on the vision of the BizOps project, to bring the product to market.

#### Requirements
* A passion for data science and analytics
* Experience with doing initial prototyping, architecture, and engineering work
* In-depth experience with Python (no Ruby or Rails experience required for this
  role)
* Experience with Kubernetes, Helm, and CI/CD is a **strict requirement**

### Database

A database specialist is a developer that focuses on database related changes
and improvements. You will spend the majority of your time making application
changes to improve database performance, availability, and reliability; though
you will also spend time working on the database infrastructure that powers
GitLab.com.

Unlike the [Database Engineer](/roles/engineering/database-engineer/) position the database
specialist title focuses more on application development and less on knowledge
of PostgreSQL. As such Ruby knowledge is absolutely required, but the
requirements for PostgreSQL knowledge / experience are less strict compared to
the Database Engineer position.

#### Example Projects

* Rewriting the database queries and related application logic used for retrieving [subgroups](https://docs.gitlab.com/ee/user/group/subgroups/index.html#subgroups)
* Rewriting code used for importing projects from other platforms (e.g. [GitHub](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14731))
* Adding trend analysis to monitoring to better detect performance and availability changes on GitLab.com
* Analyzing tables and optimizing them by adding indexes, breaking them up into separate tables, or by removing unnecessary columns.
* Reviewing database related changes submitted by other developers
* Documenting database best practices or patterns to avoid

#### Requirements

* At least 2 years of experience running PostgreSQL in production environments
* At least 5 years of experience working with Ruby
* At least 3 years of experience with Ruby on Rails or other Ruby frameworks
  such as Sinatra or Hanami
* Solid understanding of SQL
* Significant experience working in a distributed production environment

### Release Coordination

A release coordination specialist is a developer that focuses on improving the release process
and works closely with the whole Engineering team to ensure that
every GitLab release reaches the public in time.

Release Coordinator leads a [Crew](/team/structure/index.html#crew) that gets
organized per monthly release cycle and carry out the release tasks. Release Coordinator
assembles the Crew for every new release cycle. Previous Crew is disbanded by the Release Coordinator
once all release tasks are finished for the task the Crew was originally assembled for.

#### Responsibilities
* Improve the tools used to create a GitLab release
* Improve the tools used to deploy GitLab to GitLab Inc. infrastructure
* Enforce the [GitLab Release Process](https://gitlab.com/gitlab-org/release/docs)
* Select [Release Managers](/handbook/engineering/index.html#release-managers) monthly
* Select [Release Trainees](/handbook/engineering/index.html#release-managers) monthly
* Organize Release Managers and Trainees tasks
* Create release schedules
* Ensure that Release Managers and Trainees execute release schedules in time
* Communicate the release schedule clearly with others
* Develop monitoring and alerting to measure release process velocity
* Identify process bottlenecks and introduce optimizations
* Work with Product Managers to transform release tools and processes into GitLab features
* Work closely with Engineering Managers to improve teams involvement in the release
process
* Recognize process blockers and escalate to the ones responsible

### Gitter

Gitter specialists are full-stack JavaScript developers who are able to write JavaScript code than can shared between multiple environments. Gitter uses uses a JavaScript stack running node.js on the server, and bundled with webpack on the client. The iOS, Android, MacOS (Cocoa) and Linux/Windows (NW.js) clients reuse much of the same codebase but also require some knowledge of Objective-C, Swift and Java. Gitter uses MongoDB, Redis and Elasticsearch for backend storage.

#### Requirements
* Strong client-side Javascript experience
* Strong production node.js experience
* Highly desirable: MongoDB and Redis experience
* Desirable: some Java, Objective-C or Swift experience building mobile apps
* Desirable: Devops experience, working with Linux, Ansible, AWS or similar products

#### Responsibilities
* Fix prioritized issues from the issue tracker
* Triage issues (duplicates, clarification, reproduction steps, prioritization)
* Create high quality frontend and backend code
* Provide second-level support to the Production Team to ensure that all Gitter production services remain stable
* Document tribal knowledge, particularly around runbooks and production incident processes
* Keep an eye on Sentry to find regressions and ensure application errors are addressed
* Continually improve the quality of Gitter by using discretion of where you think changes are needed
* Continue to migrate the codebase from old repository locations to GitLab, while open-sourcing as much of it as possible
* Maintain the iOS, Android and desktop applications
* Provide community support for Gitter via Gitter rooms, Twitter, Zendesk, etc
* Review community contributions

## Hiring Process

Candidates for this position can generally expect a hiring process similar to the one described below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule a behavioral interview with the hiring manager
- Next, candidates will be invited to schedule a [technical interview](/handbook/hiring/interviewing/technical/) with a team manager
- Candidates will then be invited to schedule an interview with Director of Backend
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
