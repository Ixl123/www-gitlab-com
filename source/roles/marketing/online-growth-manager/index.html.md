---
layout: job_page
title: "Online Growth Manager"
---

## Intro

You might describe yourself as analytical, creative, organized, and diplomatic. You have experience delivering growth in revenue and demand by deploying a variety of crafts that span marketing, UX, and product management. You enjoy setting up conversion rate optimization experiments to continually improve the results of a prospective customers journey, and sharing results and insights with a broad group of stakeholders throughout the company. You also enjoy employing a number of marketing tactics to attract a relevant audience to a marketing site.

## Responsibilities

- Build and implement online growth strategy—understand key partnerships within organization to drive full funnel of customer acquisition, retention and upsell.
- Ensure all online marketing enhances our brand with developers, IT ops practitioners, and IT leaders. Be an expert on our audiences and their preferences.
- Be an expert on our customer lifecycle, and ways in which customer needs are evolving/how our product meets them.
- Work with product team to implement growth strategies that span into trial/product experience, and impact both customer conversion and future retention/upsell.
- Grow inbound demand tracked through GitLab’s marketing site by optimizing the customer journey:
  - Improve website conversion focused on enterprise trial requests, sales contact requests, and live chat engagement.
  - Improve conversion from trial request to download of GitLab.
  - Improve conversion from trial request to sales accepted opportunity.
- Grow trial conversion to paying customer by optimizing the customer journey:
  - Ensure trial feature usage is understood and compared to high-value customer usage
  - Improve adoption of features that prove most valuable in conversion
  - Optimize onboarding experience for engagement & activation
- Develop and execute on strategies to improve free user to paid plan conversion.
- Drive innovation and experimentation on the marketing site & trial experience to improve inbound demand creation & conversion.
- Oversee our paid social and account based advertising programs, maximizing for return on marketing spend.
- Work with data and analytics team to create standardized growth reporting and dashboards, and presenting weekly performance to the marketing department as well as stakeholders throughout the company.

## Specialties

### Search Engine Marketing

The ideal search engine marketer for GitLab undersrtands that large portions of our prospective customers prefer to research a topic on their own terms, and can apply their skill at SEO and paid search to ensure that GitLab comes up when and where it would be relevant to someone's research.
We also value meticulous tracking, testing, and analysis with a focus on continual improvement.

#### Responsibilities

Search Engine Marketing:
- Implement our SEO strategy together with our digital marketing agency.
- Grow traffic to about.gitlab.com through paid and organic search marketing programs.
- Work with marketing and sales ops to evaluate and implement adtech and martech.
- Work with our content team to improve quality scores of landing pages for paid search.
- Work with our content team to ensure the content we create on the marketing site ranks for our priority search terms.
- Report on marketing site growth from SEO and paid search.

#### Senior Responsibilities

- Define our SEO strategy together with our digital marketing agency.
- Manage our digital marketing agency which handles paid and organic search.
- Develop and manage our paid search budget.
- Improve closed-loop reporting and analysis capabilities for paid search and SEO.
